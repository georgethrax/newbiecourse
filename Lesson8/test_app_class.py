import json
from flask import Flask, current_app, make_response
from multiprocessing import Process
class TestApp(Process):
    def __init__(self, global_args, **local_args):
        self.app = Flask(__name__)
        self.app.add_url_rule("/", "index", self.index)
        self.app.add_url_rule("/", "index", self.index)
        self.app.add_url_rule("/path/to/url", "urlMethod", self.urlMethod, 
                methods=["GET"])
        self.app.add_url_rule("/formprocessing", "processForm",
                self.processForm, methods=["POST"])
        self.app.add_url_rule("/error", "error", self.error)
        self.app.add_url_rule("/return/json", 'returnjson', self.returnjson,
                methods=["GET"])
        self.global_args = global_args
        self.local_args = local_args
    def index(self, username="stranger"):
        return "Hello, " + username + "!"
    def urlMethod(self):
        return make_response("With Flask it's easy to define a URL mapping " +
                "for a method.", 200)
    def processForm(self):
        return "I can only get here via POST!"
    def error(self):
        return make_response("Here's a server-generated error.", 401)
    def returnjson(self):
        jsonData = {"Key 1": "Value 1",
                    "Key 2": "Value 2",
                    "Key 3": "Value 3"}
        jsonData = json.dumps(jsonData)
        return current_app.response_class(jsonData, mimetype="application/json")
if __name__ == "__main__":
    t = TestApp(args=(1,))
    t.start()
    #t.app.run()