import platform
import ctypes

import numpy as np


if platform.system() == 'Windows':
    dll = ctypes.cdll.LoadLibrary("./data_verifier.dll")
else:
    dll = ctypes.cdll.LoadLibrary("./data_verifier.so")

print(dll.verifier)


N=300
a = np.ndarray((N,), dtype=np.int32)
if not a.flags['C_CONTIGUOUS']:
    a = np.ascontiguousarray(a, dtype=a.dtype)  # 如果不是C连续的内存，必须强制转换
a_ctypes_ptr = ctypes.cast(a.ctypes.data, ctypes.POINTER(ctypes.c_int))   #转换为ctypes，这里转换后的可以直接利用ctypes转换为c语言中的int*，然后在c中使用

print(a_ctypes_ptr)
print(dll.verifier(a_ctypes_ptr, N))

a.sort()
print(a_ctypes_ptr)
print(dll.verifier(a_ctypes_ptr, N))