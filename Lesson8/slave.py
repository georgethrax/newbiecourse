import os
import sys
import time
import json
from multiprocessing import Process
from flask import Flask, request, jsonify


class Slave(Process):
    def __init__(self, slave_addr, slave_port, name='slave'):
        super().__init__(name=name)
        self.app = Flask(__name__)
        #self.app = app        
        self.slave_addr = slave_addr
        self.slave_port = slave_port
        print("Slave({slave_addr}, {slave_port}, {name}) init".format(slave_addr=slave_addr, slave_port=slave_port, name=name))
        self.app.add_url_rule('/', 'helo', self.helo, methods=['GET'])
        self.app.add_url_rule('/receive_data', 'receive_data', self.receive_data, methods=['POST'])
        
    def run(self):
        pid=os.getpid()
        print("{name} run. pid={pid}".format(name=self.name, pid=pid))

        self.app.run(host=self.slave_addr, port=self.slave_port, debug=None)
        
        print("app finished. name={name}, pid={pid}".format(name=self.name, pid=pid))
        time.sleep(2)
        print("{name} finish. pid={pid}".format(name=self.name, pid=pid))
    
    def helo(self):
        return 'hello from slave. pid={pid}'.format(pid=os.getpid())

    def receive_data(self):
        d = request.json()
        d['data'] = d['data'].sort()
        return jsonify(**d)
        
  

if __name__ == '__main__':
    print('main: %d'%os.getpid())
    slave = Slave('0.0.0.0', 44568, 'slave0')
    slave.start()
    slave.join()