import json
from flask import Flask, request

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def main():
    print(request.__dict__)
    print(request.get_json())
    print('data',request.data)
    print('json',request.json)
    return json.dumps({'b':'d'})
    

app.run('0.0.0.0', 60000)


print('''
curl http://node2:60000/ -d '{"a":"bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"}' -X POST -H "Content-Type: application/json"

import requests
res = requests.post('http://node2:60000/', json={"b":42342})
print(res.json())

''')

