#include<stdio.h>
#include<vector>
#include<algorithm>

using namespace std;

int sort(vector<int>& a){

    sort(a.begin(), a.end());

    return 0;
}

int main(int argc, char*argv[]){
    int N = 0;
    if (argc < 2){
        printf("invalid parameter. N needed\n");
        return 1;
    }
    sscanf(argv[1], "%d", &N);

    vector<int> a(N);
    for (int i=0; i<N; i++){
        scanf("%d", &a[i]);
    }

    sort(a);

    for (int i=0; i<N; i++){
        printf("%d ", a[i]);
    }

    return 0;
}