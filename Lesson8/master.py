import os
import sys
import time
import json
from multiprocessing import Process
from flask import Flask, request, Response
import requests
import numpy as np

ALLOWED_EXTENSIONS = set(['txt', 'json'])
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


class Master(Process):
    def __init__(self, master_addr, master_port, name='master'):
        super().__init__(name=name)
        self.app = Flask(__name__)
        self.master_addr = master_addr
        self.master_port = master_port
        self.slave_addr_port_list = []
        print("Master({master_addr}, {master_port}, {name}) init".format(master_addr=master_addr, master_port=master_port, name=name))
        self.app.add_url_rule('/', 'helo', self.helo, methods=['GET'])
        self.app.add_url_rule('/register/<string:slave_name>/<string:slave_addr>/<int:slave_port>', 'register', self.register, methods=['GET'])
        self.app.add_url_rule('/sort', 'sort', self.sort, methods=['POST', 'GET'])
        
    def run(self):
        pid=os.getpid()
        print("{name} run. pid={pid}".format(name=self.name, pid=pid))
        self.app.run(host=self.master_addr, port=self.master_port, debug=None)
        print("app finished. name={name}, pid={pid}".format(name=self.name, pid=pid))
        time.sleep(2)
        print("{name} finish. pid={pid}".format(name=self.name, pid=pid))

    def helo(self):
        return 'helo from {name}. pid = {pid}'.format(name=self.name, pid=os.getpid())

    def register(self, slave_name, slave_addr, slave_port):
        master.slave_addr_port_list.append((slave_name, slave_addr, slave_port))
        print('register Done')
        print(master.slave_addr_port_list)
        return 'OK'
    
    def sort(self):
        if request.method == 'POST':
            if 'file' not in request.files:
                print('No file part')           
                return Response(json.dumps({'status':'fail', 'message':'No file part'}),  mimetype='application/json')
            file = request.files['file']
            if file.filename == '':
                print('No selected file')
                return Response(json.dumps({'status':'fail', 'message':'No selected file'}),  mimetype='application/json')
            if not allowed_file(file.filename):
                print('Filetype not allowed')
                return Response(json.dumps({'status':'fail', 'message':'Filetype not allowed'}),  mimetype='application/json')
            if not file:
                print('No file')
                return Response(json.dumps({'status':'fail', 'message':'No file'}),  mimetype='application/json')

            file_lines = file.readlines()  #开始处理数据
            N = int(file_lines[0].strip())
            data = np.array(file_lines[1].split(), dtype=np.int32)

            d = {'N':N, 'data':data}            

            res = requests.post('http://{slave_addr}:{slave_port}/receive_data', json=d)
            ret = res.json()
            #ret = {}
            #ret['N'] = N
            #ret['data'] = np.sort(data).tolist()
            # print(ret, type(ret))
            # print('dumps')
            # print(json.dumps(ret))
            return Response(json.dumps(ret),  mimetype='application/json')
        else:
            return '''
<!doctype html>
<title>Upload new File</title>
<h1>Upload new File</h1>
<form method=post enctype=multipart/form-data>
    <p>
    <input type=file name=file>
    <input type=submit value="Upload data.txt">
    </p>
</form>'''


if __name__ == '__main__':
    master = Master('0.0.0.0', 34567, 'master')
    master.start()


