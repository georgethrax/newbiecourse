<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

# On Linux

## 编译、测试
执行脚本 `main.sh`
```
BIN_SORTER=./data_sorter
BIN_VERIFIER=./data_verifier

#build
g++ data_sorter.cpp -o $BIN_SORTER
g++ data_verifier.cpp -o $BIN_VERIFIER

#test
N=100
echo "N=$N"
python data_generator.py $N | $BIN_SORTER $N | $BIN_VERIFIER $N
```

或

```
sh main.sh
```
## 模块详解
### 数据生成
```
N=100
python data_generator.py $N #输出数据到标准输出
```

或者
```
N=100
python data_generator.py $N > data.txt #输出数据到文件
```


### 数据排序
排序程序从stdin读取数据
```
N=3
echo "3 1 2" | $BIN_SORTER $N
```

或者
```
N=100
cat data.txt | $BIN_SORTER $N > data.sorted.txt
cat data.txt
```

### 数据验证
```
N=3
echo "1 2 3" | $BIN_VERIFIER $N
echo "3 1 2" | $BIN_VERIFIER $N
```

或者

```
N=100
cat data.sorted.txt | $BIN_VERIFIER $N
```

# On Windows
假设已经安装了Visual Studio

## 打开`Developer Command Prompt for VS 2017`
可以在开始菜单里搜索。

## cd到本代码目录
```
cd /d "D:\newbiecourse\Lesson 1\lix's code"
```

## 编译c++代码
```
cl data_sorter.cpp
cl data_verifier.cpp
```

## 文件io测试
### 生成数据
```
set N=100
python data_generator.py %N% > data.txt
type data.txt
```

### 排序
```
set N=100
data_sorter.exe  %N% < data.txt > data.sorted.txt
```

### 验证
```
data_verifier.exe  %N% < data.sorted.txt
```

## 管道测试
```
set N=100
python data_generator.py %N% | data_sorter.exe %N% | data_verifier.exe %N%
```

# 在Windows中编译DLL

## 打开 `x86_x64 Cross Tools Command Prompt for VS 2017`
可以在开始菜单里搜索。

## cd到本代码目录
```
cd /d "D:\newbiecourse\Lesson 1\lix's code"
```

## 编译c++代码
```
cl /LD data_verifier.cpp
```

## 通过ctypes从python中调用dll
```
python call_data_verifier.py
```

# 在Linux编译so
```
g++ data_verifier.cpp -o data_verifier.so -shared -fPIC
```