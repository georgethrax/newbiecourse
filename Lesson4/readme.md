# 教程文件
https://github.com/georgethrax/ctr-prediction-demo

# 关于jupyter notebook的技巧
## 安装扩展插件
nbextensions
https://jupyter-contrib-nbextensions.readthedocs.io/en/latest/install.html

推荐Conda安装方式
```
conda install -c conda-forge jupyter_contrib_nbextensions
```

## 安装tex，以支持pdf导出功能
### 下载安装包
Windows/Linux: https://mirrors.tuna.tsinghua.edu.cn/CTAN/systems/texlive/Images/texlive2019-20190410.iso
MacOS: https://mirrors.tuna.tsinghua.edu.cn/CTAN/systems/mac/mactex/mactex-20190508.pkg
可以在 https://mirrors.tuna.tsinghua.edu.cn "获取下载链接"按钮处得到其他版本的安装镜像

### Linux下的安装方法
Windows/MacOS按图形界面提示操作即可。
### 挂载iso镜像文件
```
sudo mkdir /mnt/cdrom
sudo mount mount -o loop -t iso9660 texlive2019-20190410.iso /mnt/cdrom
cd /mnt/cdrom
```

### 启动安装程序，开始安装
```
./install-tl # 或 sudo ./install-tl
```
然后检查路径是否可写。按键盘`I`开始安装。

## 按提示修改`PATH`
```
export PATH="/usr/local/texlive/2019/bin/x86_64-linux:$PATH" # 修改.bashrc文件
which xelatex #检查是否成功
```

**这时就可以`Download as PDF`了，但还不支持中文**

## jupyter pdf 中文支持
### 临时
打开jupyter生成的`.tex`文件，在`\documentclass{article}`后面插入
```
\usepackage{xeCJK}
```

编译
```
xelatex notebook.tex
```

### 长期
修改 ~/anaconda3/lib/python3.6/site-packages/nbconvert/templates/latex/article.tplx，在`\documentclass{article}`后面插入`\usepackage{xeCJK}`