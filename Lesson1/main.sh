BIN_SORTER=./data_sorter.out
BIN_VERIFIER=./data_verifier.out

#build
g++ data_sorter.cpp -o $BIN_SORTER
g++ data_verifier.cpp -o $BIN_VERIFIER
g++ data_verifier.cpp -o data_verifier.so -shared -fPIC

#test
N=100
echo "N=$N"
python data_generator.py $N | $BIN_SORTER $N | $BIN_VERIFIER $N
python call_data_verifier.py