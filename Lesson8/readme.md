**注意** 本代码在Linux下运行。部分代码无法在Windows下运行（包括mingw, cygwin）

# 分布式排序

## MasterManager
### start_master(master_addr, master_port=None)
启动master，可以自动尝试可用的port
### start_slaves(slave_manager_addr, n_slaves)
事先配置好slave_manager的地址，访问SlaveManager，每个机器调用一次本函数
### stop_slaves()
访问SlaveManager，关闭slave进程
### stop_master()
关闭master进程

## SlaveManager
每个机器启动一个SlaveManager, 事先配置好master_manager的addr:port
### GET /start_slaves/<int:n_slaves>
调用start_slaves(slave_addr, n_slaves), 启动`n_slave`个Slave进程
### start_slaves(slave_addr, n_slaves, slave_port_list=None)
自动尝试可用的port
### call_master_register()
在启动时自动调用，向master注册自己的addr:port
### stop_slaves()
关闭由本SlaveManager启动的slave进程

## Master
### GET /register/<string:slave_name>/<string:slave_addr>/<int:slave_port>
接收来自slave的注册，记录slave的addr:port
### POST /sort
接收来自client的data.txt,调用call_slave_receive_data(), 将任务分配给slave(json格式)，拼接来自slave的数据并向client返回排序好的数据
### call_slave_receive_data(slave_addr, slave_port, method='POST', uri='/receive_data')
分配任务给slave，接收返回的排序好的json

## Slave

### POST /receive_data
接收来自master分配的数据(json格式)，返回排序好的数据(json格式)

## Client
### call_master_sort(master_addr, master_port, data_txt_file=sys.stdin, method='POST', uri='/sort')
将data.txt数据文件发送给master，返回data.sorted.json


# Structure
<p>
<img src='structure.jpg'>
</p>

# python库requests

# flask库之于类
```
self.app.add_url_rule("/path/to/url", "urlMethod", self.urlMethod, methods=["GET"])
```
https://www.hydrick.net/?p=47