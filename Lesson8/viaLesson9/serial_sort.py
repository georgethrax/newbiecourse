import sys
import numpy as np

if __name__ == '__main__':
    N = int(sys.argv[1])

    #data = np.ndarray((N,), dtype=np.int32)
    data = np.random.randint(9,size=(N,), dtype=np.int32)
    data.sort()
    result_data = data
    result_dict = {"N":result_data.size, "data": result_data}
    print(result_dict)