#include <stdio.h>
#include <iostream>
#include<vector>
using namespace std;

#ifdef _WIN32
#pragma message("_WIN32")
#define DLLEXPORT  __declspec(dllexport)
#else
#define DLLEXPORT 
#endif


int verify(vector<int>& a){
    int n = a.size();
    for (int i=0; i<n-1; i++){
        if (a[i] > a[i+1]){
            return 1;
        }
    }

    return 0;
}

#ifdef __cplusplus
extern "C"{
#endif
DLLEXPORT int verifier(int *a, int n){
    for (int i=0; i<n-1; i++){
        if (a[i] > a[i+1]){
            return 1;
        }
    }
    return 0;
}
#ifdef __cplusplus
}
#endif

int main(int argc, char*argv[]){ 

    int N = 0;
    if (argc < 2){
        printf("invalid parameter. N needed\n");
        return 1;
    }
    sscanf(argv[1], "%d", &N);

    vector<int> a(N);
    for (int i=0; i<N; i++){
        scanf("%d", &a[i]);
    }

    int flag_sorted = verify(a);
    //int *pa = &a[0];
    //int flag_sorted = verifier(pa, a.size());

    if (flag_sorted != 0){
        printf("Not Sorted\n");
    }
    else{
        printf("Sorted\n");
    }

    return 0;
}