import os
from flask import Flask, flash, request, redirect, session 
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = 'tmp/data'  #上传文件所保存的文件夹
ALLOWED_EXTENSIONS = set(['txt'])
os.makedirs(UPLOAD_FOLDER, exist_ok=True) #确保文件夹存在
#os.popen('mkdir -p {}'.format(UPLOAD_FOLDER)) 

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SECRET_KEY'] = os.urandom(24)


# 排序和验证
def call_sort_verify(filepath_save):
    BIN_SORTER='./data_sorter'
    BIN_VERIFIER='./data_verifier'    
    proc = os.popen("N=`wc -w < {filepath_save}` && {BIN_SORTER} $N < {filepath_save} | {BIN_VERIFIER} $N".format(BIN_SORTER=BIN_SORTER, filepath_save=filepath_save, BIN_VERIFIER=BIN_VERIFIER) ) #执行shell命令
    output = proc.read() #读取输出流
    return output

@app.route('/sort_verify', methods=['GET'])
def sort_verify():
    print("sorting...")
    print(session)
    filepath_save = session['filepath_save']    
    print(filepath_save)
    return call_sort_verify(filepath_save)




# 处理文件上传
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/upload_data', methods=['GET', 'POST'])
def upload_file():
    print(request.__dict__)
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            print('No file part')           
            return redirect(request.url)
        
        file = request.files['datafile']
        # N = int(request.json['N'])
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            print('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filename_save = filename + '.' + request.environ['REMOTE_ADDR'] + '.txt'
            filepath_save = os.path.join(app.config['UPLOAD_FOLDER'], filename_save)
            session['filepath_save'] = filepath_save            
            file.save(filepath_save) #将用户上传的文件保存到本地磁盘
            return redirect('/sort_verify') #执行排序和验证，返回验证结果字符串
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
        <input type=file name=datafile>
        <input type=submit value=Upload>
    </form>

    '''


@app.route('/', methods=['GET'])
def main():
    return redirect('/upload_data')


