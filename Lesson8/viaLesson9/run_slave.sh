export FLASK_APP=app.py
export FLASK_ENV=development

# 在后台启动3各slave进程。注意地址和端口要和config.yaml文件一致
python -m flask run --host=localhost --port=60000  &
python -m flask run --host=localhost --port=60001  &
python -m flask run --host=localhost --port=60002  &
