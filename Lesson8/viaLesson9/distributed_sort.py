import sys
import yaml
import requests
import numpy as np
import os
import time
import multiprocessing

# 远程调用
def call_slave_receive_data(args):
    #print(args)
    print('pid = %d, time = %d'%(os.getpid(), time.time()))
    _data, _config = args
    #print(config)
    res = requests.post("http://{addr}:{port}/sort".format_map(_config), json={"N":_data.size, "data":_data.tolist()})
    data_sorted = res.json()['data']
    print('end. pid = %d, time = %d'%(os.getpid(), time.time()))
    return np.array(data_sorted, dtype=np.int32)


if __name__ == '__main__':
    # 生成数据

    N = int(sys.argv[1])
    M = int(sys.argv[2])

    #data = np.ndarray((N,), dtype=np.int32)
    data = np.random.randint(9,size=(N,), dtype=np.int32)

    # 按值域划分

    data_partitions = [0] * M
    min_value = data.min()
    max_value = data.max()

    value_linspace = np.linspace(min_value, max_value+1, M+1)

    # for i in range(M):
    #     #f = filter(lambda x: x >= value_linspace[i] and x < value_linspace[i+1], data)
    #     #data_partitions[i] = np.fromiter(f, dtype=np.int32)
    #     data_partitions[i] = data[(data >= value_linspace[i]) & (data < value_linspace[i+1]) ]

    data_partitions = list(map(lambda i: data[(data >= value_linspace[i]) & (data < value_linspace[i+1]) ], range(M)))



    config = yaml.load(open('config.yaml'))

    pool = multiprocessing.Pool(M)
    result_partitions = list(pool.map(call_slave_receive_data, zip(data_partitions, config)))
    pool.close()
    pool.join()
    #result_partitions = list(map(call_slave_receive_data, zip(data_partitions, config)))

    # 拼装结果
    result_data = np.concatenate(result_partitions)
    result_dict = {"N":result_data.size, "data": result_data}
    print(result_dict)