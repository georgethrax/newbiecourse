import os
import sys
import time
import yaml

import requests
from flask import Flask

from slave import Slave

app = Flask(__name__)

@app.route('/', methods=['GET'])
def helo():
    return 'hello from slave_manager'

@app.route('/start_slaves/<int:n_slaves>', methods=['GET'])
def start_slaves(n_slaves):
    slave_list = slave_manager.start_slaves(n_slaves)
    slave_manager.call_master_register() #向master注册
    return str(slave_list)

@app.route('/stop_slaves', methods=['GET'])
def stop_slaves():
    slave_manager.stop_slaves()
    return 'Done'

class SlaveManager:
    def __init__(self, master_addr='localhost', master_port=None, slave_manager_addr='localhost', slave_manager_port=None):
        self.master_addr = master_addr
        self.master_port = master_port
        self.slave_manager_addr = slave_manager_addr
        self.slave_manager_port = slave_manager_port
        self.slave_list=[]

    def run(self):
        app.run(host=self.slave_manager_addr, port=self.slave_manager_port, debug=None)

    def start_slaves(self, n_slaves):
        port_start = self.slave_manager_port + 1
        for i in range(0, n_slaves):
            slave = Slave(self.slave_manager_addr, port_start + i, "slave{i}".format(i=i))
            slave.start()
            self.slave_list.append(slave)
        ret = [(slave.name, slave.slave_addr, slave.slave_port) for slave in self.slave_list]
        print(ret)
        return ret
    
    def call_master_register(self):
        for slave in self.slave_list:
            res = requests.get('http://{master_addr}:{master_port}/register/{slave_name}/{slave_addr}/{slave_port}'.format(
                master_addr=self.master_addr,
                master_port=self.master_port,
                slave_name=slave.name,
                slave_addr = slave.slave_addr,
                slave_port=slave.slave_port
            ))        
    
    def stop_slaves(self):
        for slave in self.slave_list:
            slave.terminate()

        

config = yaml.load(open('config.yaml'))
master_addr = config.get('master_addr', 'node2')
master_port = config.get('master_port', None)
slave_manager_addr = config.get('slave_manager_addr', 'node2')
slave_manager_port = config.get('slave_manager_port', None)

slave_manager = SlaveManager(master_addr, master_port, slave_manager_addr, slave_manager_port)

if __name__ == '__main__':            
    slave_manager.run()