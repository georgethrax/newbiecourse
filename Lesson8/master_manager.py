import os
import sys
import time
import yaml

import requests

from master import Master

class MasterManager:
    def __init__(self, master_addr='localhost', master_port=None, slave_manager_addr='localhost', slave_manager_port=None, n_slaves=3):
        self.master_addr = master_addr
        self.master_port = master_port
        self.slave_manager_addr = slave_manager_addr
        self.slave_manager_port = slave_manager_port
        self.master = Master(master_addr, master_port, 'master@{host}'.format(host=master_addr))

    def start_master(self):
        self.master.start()
        print('MasterManager. pid={pid}'.format(pid=os.getpid()))

    def start_slaves(self):
        res = requests.get("http://{slave_manager_addr}:{slave_manager_port}/start_slaves/{n_slaves}".format(
            slave_manager_addr=self.slave_manager_addr,
            slave_manager_port=self.slave_manager_port,
            n_slaves=n_slaves
        ))
    



if __name__ == '__main__':            
    config = yaml.load(open('config.yaml'))
    master_addr = config.get('master_addr', 'node2')
    master_port = config.get('master_port', None)
    slave_manager_addr = config.get('slave_manager_addr', 'node2')
    slave_manager_port = config.get('slave_manager_port', None)
    n_slaves = config.get('n_slaves', 3)

    master_manager = MasterManager(master_addr, master_port, slave_manager_addr, slave_manager_port, n_slaves)
    master_manager.start_master()