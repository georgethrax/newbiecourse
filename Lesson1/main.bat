cl data_sorter.cpp
cl data_verifier.cpp

set N=100
rem python data_generator.py %N% > data.txt
rem type data.txt

rem data_sorter.exe  %N% < data.txt > data.sorted.txt

rem data_verifier.exe  %N% < data.sorted.txt

python data_generator.py %N% | data_sorter.exe %N% | data_verifier.exe %N%