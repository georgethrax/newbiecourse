#include <Python.h> //包含头文件，在c++中嵌入python，这是必须的
int main()
{
 Py_Initialize();

 PyObject * pModule = NULL;
 PyObject * pFunc   = NULL;

 pModule = PyImport_ImportModule("test");
 pFunc   = PyObject_GetAttrString(pModule, "Hello");
 PyEval_CallObject(pFunc, NULL);

 Py_Finalize();

 return 0;
}